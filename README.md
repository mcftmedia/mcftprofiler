# McftProfiler

McftProfiler empowers your staff, giving them the ability to note users, see a users alternative accounts, manage user reputation, award users, and do so much more.

This was made to suit the needs of the [Diamond Mine](http://diamondmine.net) server.

**[License](https://diamondmine.net/plugins/license)** -- **[Issues](https://mcftmedia.atlassian.net/issues/?jql=project=PROFILER)** --  **[Builds](https://mcftmedia.atlassian.net/builds/browse/PROFILER-PROFILER)**
-- **[Download](http://diamondmine.net/plugins/download/McftProfiler)** -- **[Bukkit Dev](http://dev.bukkit.org/server-mods/mcftprofiler/)**