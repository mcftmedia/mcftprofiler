package net.diamondmine.mcftprofiler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * Handler for IP logging.
 * 
 * @author Jon la Cour
 * @version 1.3.1
 */
public class IPHandler extends McftProfiler {
    private static final int MAX_IPS_LENGTH = 242;

    /**
     * This fetches a users IP address. It will try to get the users IP even if
     * they're offline using the IP tracking feature.
     * 
     * @param name
     *            The user to fetch an IP from
     * @return The users IP or Unknown/Offline
     * @since 1.0.0
     */
    public static final String fetchIP(final String name) {
        FileConfiguration config = Configuration.getConfiguration();
        Player player = Bukkit.getServer().getPlayer(name);
        if (player != null && player.isOnline()) {
            String ipaddress = player.getAddress().toString().substring(1).split(":")[0];
            if (ipaddress.contains("/")) {
                ipaddress = ipaddress.split("/")[1];
            }
            return ipaddress;
        } else {
            boolean iptracking = config.getBoolean("profile.smart-tracking");
            if (iptracking) {
                PreparedStatement query = null;
                ResultSet result = null;
                try {
                    try {
                        query = database.db.prepare("SELECT ip, users, ips FROM " + database.prefix + "iplog WHERE users LIKE ? LIMIT 1");
                        query.setString(1, name);
                        result = query.executeQuery();

                        if (result.next()) {
                            return result.getString("ip");
                        }
                    } catch (Exception e) {
                        log("Error while fetching IP of user: " + e.getLocalizedMessage(), "warning");
                    }
                } finally {
                    try {
                        query.close();
                        result.close();
                    } catch (Exception e) {
                        log("Error closing queries while fetching IP of user: " + e.getLocalizedMessage(), "severe");
                    }
                }
            } else {
                return "Offline";
            }
            return "Unknown";
        }
    }

    /**
     * This is the main function for IP logging. This relates users with their
     * IPs, previous IPs, and other users.
     * 
     * @param ip
     *            The users IP to be logged
     * @param name
     *            The users name to be logged
     * @since 1.0.0
     */
    public static void logIP(final String ip, final String name) {
        try {
            ResultSet ipcheck = database.db.query("SELECT ipid, ip, users, ips FROM " + database.prefix + "iplog WHERE ip LIKE '" + ip + "' LIMIT 10;");

            boolean hasLogged = ipcheck.next();
            String ipid = "0";
            String ips = "";

            if (hasLogged) {
                String accounts = ipcheck.getString("users");
                ipid = ipcheck.getString("ipid");
                ips = ipcheck.getString("ips");
                ipcheck.close();

                if (ips != null) {
                    if (ips.length() >= MAX_IPS_LENGTH) {
                        ips = ips.substring(ips.lastIndexOf(",") + 1, ips.length());
                        database.db.query("UPDATE " + database.prefix + "iplog SET ips = '" + ips + "' WHERE ipid = " + ipid + ";");
                    }
                }

                boolean hasname = false;
                if (accounts != null) {
                    String[] users = accounts.split(",");
                    StringBuilder result = new StringBuilder();
                    if (users.length > 0) {
                        result.append(users[0]);
                        for (int i = 1; i < users.length; i++) {
                            if (!users[i].equals(name)) {
                                result.append(",");
                                result.append(users[i]);
                            } else {
                                hasname = true;
                            }
                        }
                        if (!users[0].equals(name)) {
                            if (!hasname) {
                                result.append(",").append(name);
                            }
                        }
                        accounts = result.toString();
                        database.db.query("UPDATE " + database.prefix + "iplog SET users = '" + accounts + "' WHERE ipid = " + ipid + ";");
                    }
                } else {
                    database.db.query("UPDATE " + database.prefix + "iplog SET users = '" + name + "' WHERE ipid = " + ipid + ";");
                }
            } else {
                ipcheck.close();
                database.db.query("INSERT INTO " + database.prefix + "iplog (ipid, ip, users, ips) VALUES (NULL, '" + ip + "', '" + name + "', NULL);");
            }

            ResultSet similar = database.db.query("SELECT ipid, ip, users, ips FROM " + database.prefix + "iplog WHERE ip NOT LIKE '" + ip
                    + "' AND users LIKE '%" + name + "%' LIMIT 10;");
            if (similar.next()) {
                String sipid = similar.getString("ipid");
                String host = similar.getString("ip");
                String related = similar.getString("ips");
                similar.close();
                if (related != null) {
                    if (!related.contains(ip)) {
                        if (related.length() >= MAX_IPS_LENGTH) {
                            related = related.substring(related.lastIndexOf(",") + 1, related.length());
                        }
                        StringBuilder result = new StringBuilder();
                        result.append(related).append(",").append(ip);
                        related = result.toString();
                        database.db.query("UPDATE " + database.prefix + "iplog SET ips = '" + related + "' WHERE ipid = " + sipid + ";");
                    }
                } else {
                    database.db.query("UPDATE " + database.prefix + "iplog SET ips = '" + ip + "' WHERE ipid = " + sipid + ";");
                }
                if (hasLogged) {
                    if (ips != null) {
                        if (!ips.contains(host)) {
                            if (ips.length() >= MAX_IPS_LENGTH) {
                                ips = ips.substring(ips.lastIndexOf(",") + 1, ips.length());
                            }
                            StringBuilder result = new StringBuilder();
                            result.append(ips).append(",").append(host);
                            ips = result.toString();
                            database.db.query("UPDATE " + database.prefix + "iplog SET ips = '" + ips + "' WHERE ipid = " + ipid + ";");
                        }
                    } else {
                        database.db.query("UPDATE " + database.prefix + "iplog SET ips = '" + host + "' WHERE ipid = " + ipid + ";");
                    }
                }
            } else {
                similar.close();
            }
        } catch (Exception e) {
            log("Error while logging user IP: " + e.getLocalizedMessage(), "warning");
        }
    }
}
