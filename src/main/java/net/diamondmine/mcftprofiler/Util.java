package net.diamondmine.mcftprofiler;

import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Utility methods.
 * 
 * @author Jon la Cour
 * @version 1.3.0
 */
public class Util extends McftProfiler {

    /**
     * This replaces all default color codes in text with Bukkit ChatColors.
     * 
     * @param m
     *            The message to fix
     * @return A string with Bukkit ChatColors
     * @since 1.0.0
     */
    public static String fixColor(final String m) {
        String s = m;
        s = s.replace("&4", ChatColor.DARK_RED + "");
        s = s.replace("&c", ChatColor.RED + "");
        s = s.replace("&6", ChatColor.GOLD + "");
        s = s.replace("&e", ChatColor.YELLOW + "");
        s = s.replace("&2", ChatColor.DARK_GREEN + "");
        s = s.replace("&a", ChatColor.GREEN + "");
        s = s.replace("&b", ChatColor.AQUA + "");
        s = s.replace("&3", ChatColor.DARK_AQUA + "");
        s = s.replace("&1", ChatColor.DARK_BLUE + "");
        s = s.replace("&9", ChatColor.BLUE + "");
        s = s.replace("&d", ChatColor.LIGHT_PURPLE + "");
        s = s.replace("&5", ChatColor.DARK_PURPLE + "");
        s = s.replace("&f", ChatColor.WHITE + "");
        s = s.replace("&7", ChatColor.GRAY + "");
        s = s.replace("&8", ChatColor.DARK_GRAY + "");
        s = s.replace("&0", ChatColor.BLACK + "");
        s = s.replace("&k", ChatColor.MAGIC + "");
        return s;
    }

    /**
     * This makes a human-readable pretty date.
     * 
     * @param date
     *            The date to format
     * @return A pretty date string
     * @since 1.1.2
     */
    public static String prettyDate(final Date date) {
        long current = (new Date()).getTime(), timestamp = date.getTime(), diff = (current - timestamp) / 1000;
        int amount = 0;
        String what = "";
        if (diff > 31536000) {
            amount = (int) (diff / 31536000);
            what = "year";
        } else if (diff > 2629744) {
            amount = (int) (diff / 2629744);
            what = "month";
        } else if (diff > 604800) {
            amount = (int) (diff / 604800);
            what = "week";
        } else if (diff > 86400) {
            amount = (int) (diff / 86400);
            what = "day";
        } else if (diff > 3600) {
            amount = (int) (diff / 3600);
            what = "hour";
        } else if (diff > 60) {
            amount = (int) (diff / 60);
            what = "minute";
        } else {
            amount = (int) diff;
            what = "second";
            if (amount < 60) {
                return "Just now";
            }
        }

        if (amount == 1) {
            if (what.equals("day")) {
                return "Yesterday";
            } else if (what.equals("week") || what.equals("month") || what.equals("year")) {
                return "Last " + what;
            }
        } else {
            what += "s";
        }

        return amount + " " + what + " ago";
    }

    /**
     * Returns a boolean for whether or not a user is currently online.
     * 
     * @param name
     *            The users name to check.
     * @return boolean
     * @since 1.1.3
     */
    public static boolean isOnline(final String name) {
        return Bukkit.getServer().getPlayer(name) != null;
    }
}
