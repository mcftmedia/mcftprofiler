package net.diamondmine.mcftprofiler;

import static net.diamondmine.mcftprofiler.Util.fixColor;

import java.sql.ResultSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;

/**
 * Handler for Jail information.
 * 
 * @author Jon la Cour
 * @version 1.4.0
 */
public class Jail extends McftProfiler {

    /**
     * Send the profile line to the requesting player.
     * 
     * @param name
     *            Who to lookup.
     * @param sender
     *            The requesting player.
     * @since 1.3.0
     */
    public static void send(final String name, final CommandSender sender) {
        jailStatus(name, sender);
    }

    /**
     * This checks a players jail status.
     * 
     * @param name
     *            The user to check
     * @param sender
     *            Command sender
     * @since 1.0.0
     */
    public static void jailStatus(final String name, final CommandSender sender) {
        ResultSet jail = null;
        try {
            if (database.db.checkTable("jail_prisoners")) {
                jail = database.db.query("SELECT RemainTime, Jailer FROM jail_prisoners WHERE PlayerName LIKE '" + name + "';");
                boolean jrows = jail.next();
                if (jrows) {
                    String timeleft = jail.getString("RemainTime");
                    String jailer = jail.getString("Jailer");
                    if (timeleft != null && jailer != null) {
                        String time = jailTime(Integer.parseInt(timeleft) / 6);
                        World world = Bukkit.getServer().getWorlds().get(0);
                        String rank = c.getPrimaryGroup(world, jailer);
                        String prefix = fixColor(c.getGroupPrefix(world, rank));

                        if (time.equals("never")) {
                            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Jail: " + ChatColor.BLUE + "Permanently jailed.");
                        } else {
                            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Jailed: " + ChatColor.BLUE + "Free in " + time + ChatColor.WHITE
                                    + " By: " + prefix + jailer);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log("Error while fetching Jail time for user: " + e.getLocalizedMessage(), "warning");
        } finally {
            try {
                jail.close();
            } catch (Exception e) {
                log("Error closing query while fetching Jail time for user: " + e.getLocalizedMessage(), "severe");
            }
        }
    }

    /**
     * This changes a jail time (minutes) into a easy to read format.
     * 
     * @param minutes
     *            Minutes of jail time
     * @return An easy to read jail time
     * @since 1.0.0
     */
    private static String jailTime(final int minutes) {
        int hours = minutes / 60;
        int mins = minutes % 60;
        if (hours > 0) {
            return hours + "h " + mins + "m";
        } else if (mins > 0) {
            return mins + "m";
        } else if (mins <= 0) {
            return "never";
        } else {
            return "<1m";
        }
    }

}
