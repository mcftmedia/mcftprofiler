package net.diamondmine.mcftprofiler;

import java.io.IOException;

import net.diamondmine.geoip.LookupService;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;


/**
 * Handler for GeoIP lookups.
 * 
 * @author Jon la Cour
 * @version 1.4.0
 */
public class GeoIP extends McftProfiler {

    /**
     * Send the profile line to the requesting player.
     * 
     * @param name
     *            Who to lookup.
     * @param sender
     *            The requesting player.
     * @since 1.3.0
     */
    public static void send(final String name, final CommandSender sender) {
        String country = locateIP(IPHandler.fetchIP(name));

        if (p.has(Bukkit.getServer().getWorlds().get(0), name, "mcftprofiler.country.hide")) {
            return;
        }

        sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Country: " + ChatColor.BLUE + country);
    }

    /**
     * This geographically locates an IP address using the GeoIP database.
     * 
     * @param ip
     *            The IP to geographically locate
     * @return A country or Local/Unknown
     * @since 1.0.0
     */
    public static String locateIP(final String ip) {
        if (ip.equals("127.0.0.1")) {
            return "Local";
        }

        String country = "Unknown";
        try {
            FileConfiguration config = Configuration.getConfiguration();
            String dbfile = config.getString("profile.geoip.file", "/usr/local/share/GeoIP/GeoIP.dat");

            LookupService cl = new LookupService(dbfile, LookupService.GEOIP_STANDARD);
            country = cl.getCountry(ip).getName().toString();
            cl.close();
        } catch (IOException e) {
            log("Country database not found, check the configuration setting for profile.geoip.file: " + e.getLocalizedMessage(), "severe");
        }

        return country;
    }

}
