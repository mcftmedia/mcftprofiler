package net.diamondmine.mcftprofiler;

import static net.diamondmine.mcftprofiler.Util.fixColor;
import static net.diamondmine.mcftprofiler.Util.isOnline;
import static net.diamondmine.mcftprofiler.Util.prettyDate;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * Handler for profiles and related accounts.
 * 
 * @author Jon la Cour
 * @version 1.4.0
 */
public class Profile extends McftProfiler {

    /**
     * Sends a player another player's profile.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            The command arguments.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.4
     */
    public static void sendProfile(final CommandSender sender, final String[] args, final String cmdname) {
        FileConfiguration config = Configuration.getConfiguration();
        boolean mainnode = false, relatednode = false, reputationnode = false, console = false;
        Player player = null;
        String pname = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            pname = player.getName();
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            } else if (p.has(player, "mcftprofiler.related.*")) {
                relatednode = true;
            } else if (p.has(player, "mcftprofiler.reputation.*")) {
                reputationnode = true;
            }
        } else {
            pname = "Console";
            mainnode = true;
            console = true;
        }
        if (mainnode || p.has(player, "mcftprofiler.status.*") || p.has(player, "mcftprofiler.status")) {
            if (args.length == 1 || args.length == 0) {
                String name = "";
                if (args.length == 0) {
                    name = pname;
                } else {
                    name = args[0];
                }
                Player user = Bukkit.getServer().getPlayer(name);
                if (user != null) {
                    name = user.getName();
                }
                Location uloc = null;
                if (user != null) {
                    uloc = user.getLocation();
                }
                String error = "exception when checking if user exists: ";
                boolean prows = false;
                try {
                    ResultSet profile = database.db.query("SELECT profileid, username FROM " + database.prefix + "profiles WHERE username = '" + name
                            + "' LIMIT 10;");
                    prows = profile.next();
                    profile.close();
                } catch (Exception e) {
                    log(e.toString() + error + e.getMessage(), "severe");
                }
                if (!prows && user == null) {
                    sender.sendMessage(ChatColor.GOLD + "Sorry, we don't know that user. You entered " + name + ".");
                } else {
                    String world = Bukkit.getServer().getWorlds().get(0).getName();
                    if (sender instanceof Player) {
                        world = player.getLocation().getWorld().getName();
                        if (world == null) {
                            world = Bukkit.getServer().getWorlds().get(0).getName();
                        }
                    }
                    
                    String rank = c.getPrimaryGroup(world, name);
                    String prefix = fixColor(c.getGroupPrefix(world, rank));
                    String title = "";
                    String reputation = "";
                    
                    if (!config.getBoolean("profile.rank", true)) {
                        rank = "";
                    }

                    if (config.getBoolean("profile.title", true)) {
                        try {
                            if (!name.equals(user.getDisplayName())) {
                                title = ChatColor.RED + " [" + ChatColor.WHITE + prefix + user.getDisplayName() + ChatColor.RED + "]";
                            }
                        } catch (NullPointerException e) {
                            title = "";
                        }
                    }

                    if (config.getBoolean("profile.reputation")) {
                        if (mainnode || reputationnode || p.has(player, "mcftprofiler.reputation")) {
                            reputation = Reputation.getReputation(name);
                        }
                    }

                    String nameLine = ChatColor.DARK_RED + "* " + prefix + name + title + ChatColor.WHITE + " " +  rank;
                    String reputationLine = ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Reputation: " + reputation;

                    sender.sendMessage(nameLine);
                    if (config.getBoolean("profile.reputation")) {
                        sender.sendMessage(reputationLine);
                    }
                    lastOn(name, sender);
                    if (config.getBoolean("profile.smart-tracking")) {
                        if (mainnode || relatednode || p.has(player, "mcftprofiler.related")) {
                            String related = relatedAccounts(name, sender);
                            if (!p.has(world, name, "mcftprofiler.related.hide")) {
                                if (!related.isEmpty()) {
                                    sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "May also be: " + related);
                                }
                            }
                        }
                    }
                    if (mainnode || p.has(player, "mcftprofiler.ip.info")) {
                        String address = IPHandler.fetchIP(name);
                        if (p.has(world, name, "mcftprofiler.ip.hide")) {
                            address = "Unknown";
                        }
                        sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Peer address: " + ChatColor.BLUE + address);
                    }
                    if (config.getBoolean("profile.geoip.enabled") && (mainnode || p.has(player, "mcftprofiler.country"))) {
                        GeoIP.send(name, sender);
                    }
                    if (config.getBoolean("profile.jail-info.enabled") && (mainnode || p.has(player, "mcftprofiler.jail"))) {
                        Jail.send(name, sender);
                    }
                    if (mainnode) {
                        Notes.send(name, sender);
                    } else if (p.has(player, "mcftprofiler.notes")) {
                        Notes.send(name, sender, player);
                    }
                    if (uloc != null) {
                        if (console || p.has(player, "mcftprofiler.location.*") || p.has(player, "mcftprofiler.location")) {
                            String message = uloc.getWorld().getName();
                            if (console || p.has(player, "mcftprofiler.location.*") || p.has(player, "mcftprofiler.location.cords")) {
                                message = uloc.getBlockX() + " " + uloc.getBlockY() + " " + uloc.getBlockZ() + " in " + uloc.getWorld().getName();
                            }
                            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Location: " + ChatColor.BLUE + message);
                        }
                    }
                    if (config.getBoolean("profile.awards")) {
                        Awards.send(name, sender);
                    }
                }
            } else {
                sender.sendMessage(ChatColor.GOLD + "Please specify a username.");
                sender.sendMessage(ChatColor.RED + "/" + cmdname + " " + ChatColor.GRAY + "name " + ChatColor.YELLOW + "View a users profile.");
            }
        } else {
            sender.sendMessage(ChatColor.GOLD + "You don't have permission to do that!");
        }
    }

    /**
     * This creates user profiles for awards, etc.
     * 
     * @param ip
     *            The users IP to be used
     * @param name
     *            The users name to be used
     * @since 1.0.0
     */
    public static void checkProfile(final String ip, final String name) {
        String error = "exception when creating user profile: ";
        try {
            ResultSet profile = database.db.query("SELECT profileid, username, ip, awards FROM " + database.prefix + "profiles WHERE username LIKE '" + name
                    + "' LIMIT 10;");
            boolean prows = profile.next();
            if (prows) {
                String profileid = profile.getString("profileid");
                String host = profile.getString("ip");
                profile.close();
                if (ip != null && host != null) {
                    if (host.equals(ip)) {
                        return;
                    }
                    database.db.query("UPDATE " + database.prefix + "profiles SET ip = '" + ip + "' WHERE profileid = " + profileid + ";");
                }
            } else {
                profile.close();
                database.db.query("INSERT INTO " + database.prefix + "profiles (profileid, username, ip, awards) VALUES (NULL, '" + name + "', '" + ip
                        + "', NULL);");
            }
        } catch (Exception e) {
            log(e.toString() + error + e.getMessage(), "severe");
        }
    }

    /**
     * This fetches the last on time for a player.
     * 
     * @param name
     *            The user to fetch the last on time from
     * @param sender
     *            Command sender
     * @since 1.1.2
     */
    public static void lastOn(final String name, final CommandSender sender) {
        boolean online = isOnline(name);
        if (online) {
            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Last on: " + ChatColor.GREEN + "Online now");
        } else {
            String error = "exception when fetching users last on time: ";
            try {
                ResultSet laston = database.db.query("SELECT laston FROM " + database.prefix + "profiles WHERE username LIKE '" + name
                        + "' ORDER BY laston DESC");
                boolean trows = laston.next();
                if (trows) {
                    do {
                        try {
                            Timestamp time = Timestamp.valueOf(laston.getString("laston"));
                            Date date = time;
                            String prettytime = prettyDate(date);
                            String normaltime = new SimpleDateFormat("d MMM h:mma z").format(time);
                            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Last on: " + ChatColor.BLUE + prettytime + ChatColor.WHITE + " ("
                                    + normaltime + ")");
                        } catch (Exception e) {
                            sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Last on: " + ChatColor.BLUE + "Unknown");
                        }
                    } while (laston.next());
                    laston.close();
                } else {
                    laston.close();
                    sender.sendMessage(ChatColor.DARK_RED + "- " + ChatColor.WHITE + "Last on: " + ChatColor.BLUE + "Never");
                }
            } catch (Exception e) {
                log(e.toString() + error + e.getMessage(), "severe");
            }
        }
    }

    /**
     * This is used in the status command to show related users.
     * 
     * @param name
     *            The users name to check
     * @param sender
     *            Command sender
     * @return The related users
     * @since 1.0.0
     */
    public static String relatedAccounts(final String name, final CommandSender sender) {
        try {
            ResultSet relatedusers = database.db.query("SELECT ip, users, ips FROM " + database.prefix + "iplog WHERE users LIKE '%" + name + "%';");
            boolean rrows = relatedusers.next();
            if (rrows) {
                List<String> users = new ArrayList<String>();
                String world;
                Player player;
                if (sender instanceof Player) {
                    player = (Player) sender;
                    world = player.getLocation().getWorld().getName();
                } else {
                    world = Bukkit.getServer().getWorlds().get(0).getName();
                }

                do {
                    String userrow = relatedusers.getString("users");
                    if (userrow != null) {
                        if (userrow.contains(",")) {
                            String[] s = userrow.split(",");
                            users.addAll(Arrays.asList(s));
                            users.remove(name);
                        } else {
                            users.add(userrow);
                            users.remove(name);
                        }
                    }
                } while (relatedusers.next());
                relatedusers.close();

                List<String> prefixedusers = new ArrayList<String>();

                Iterator<String> itr = users.iterator();
                while (itr.hasNext()) {
                    String user = itr.next().toString();
                    String rank = c.getPrimaryGroup(world, user);
                    String prefix = fixColor(c.getGroupPrefix(world, rank));
                    String u = prefix + user;
                    prefixedusers.add(u);
                }

                // A lovely way to remove duplicate usernames.
                HashSet<String> tempusers = new HashSet<String>();
                tempusers.addAll(prefixedusers);
                prefixedusers.clear();
                prefixedusers.addAll(tempusers);
                StringBuilder sb = new StringBuilder();
                for (String s : prefixedusers) {
                    sb.append(s);
                    sb.append(ChatColor.WHITE).append(", ");
                }
                String related = "";
                if (sb.length() > 0) {
                    related = sb.toString();
                    related = related.substring(0, sb.length() - 2);
                }

                return related;
            } else {
                relatedusers.close();
            }
        } catch (Exception e) {
            log(e.toString() + "exception when checking related accounts for user: " + e.getMessage(), "severe");
        }
        return "";
    }

    /**
     * Sends a player another player's profile.
     * 
     * @param sender
     *            The command sender.
     * @param args
     *            The command arguments.
     * @param cmdname
     *            The name of the command used.
     * @since 1.2.5
     */
    public static void gotoLastPos(final CommandSender sender, final String[] args, final String cmdname) {
        boolean mainnode = false, console = false;
        Player player = null;
        if (sender instanceof Player) {
            player = (Player) sender;
            if (p.has(player, "mcftprofiler.*")) {
                mainnode = true;
            }
        } else {
            mainnode = true;
            console = true;
        }
        if (mainnode || p.has(player, "mcftprofiler.lastpos.*") || p.has(player, "mcftprofiler.lastpos")) {
            String name = "";
            if (args.length != 0) {
                name = args[0];
            } else {
                sender.sendMessage(ChatColor.GOLD + "Please specify a player's name.");
                return;
            }
            String position = LastPosition.getLastPos(name);
            if (position == null || position.isEmpty()) {
                sender.sendMessage(ChatColor.GOLD + "Unable to find player's last location.");
            } else {
                if (console) {
                    sender.sendMessage(ChatColor.GOLD + name + "'s last position: " + LastPosition.getString(position));
                } else {
                    World world = Bukkit.getServer().getWorld(LastPosition.getWorld(position));
                    String seed = LastPosition.getSeed(position);
                    if (Long.toString(world.getSeed()).equals(seed)) {
                        Double x = Double.valueOf(LastPosition.getX(position));
                        Double y = Double.valueOf(LastPosition.getY(position));
                        Double z = Double.valueOf(LastPosition.getZ(position));

                        Location location = new Location(world, x, y, z);
                        if (!world.isChunkLoaded(location.getBlockX() >> 4, location.getBlockZ() >> 4)) {
                            world.loadChunk(location.getBlockX() >> 4, location.getBlockZ() >> 4);
                        }
                        player.teleport(location);
                    } else {
                        LastPosition.removeLastPos(name);
                        sender.sendMessage(ChatColor.GOLD + "Unable to find player's last location.");
                    }
                }
            }
        }
    }
}
