package net.diamondmine.mcftprofiler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.bukkit.World;

/**
 * Parser for last positions.
 * 
 * @author Jon la Cour
 * @version 1.3.1
 */
public class LastPosition extends Profile {

    /**
     * Fetches a player's last position.
     * 
     * @param name
     *            The player's name.
     * @return Null or last position.
     * @since 1.2.5
     */
    public static String getLastPos(final String name) {
        PreparedStatement query = null;
        ResultSet result = null;
        try {
            query = database.db.prepare("SELECT lastpos FROM " + database.prefix + "profiles WHERE username = ? LIMIT 1;");
            query.setString(1, name);
            result = query.executeQuery();

            if (result.next()) {
                return result.getString("lastpos");
            }
        } catch (Exception e) {
            log("Error while fetching users last position: " + e.getLocalizedMessage(), "warning");
        } finally {
            try {
                query.close();
                result.close();
            } catch (Exception e) {
                log("Error closing queries while fetching users last position: " + e.getLocalizedMessage(), "severe");
            }
        }

        return "";
    }

    /**
     * Logs a player's last position.
     * 
     * @param name
     *            The player's name.
     * @param x
     *            X location.
     * @param y
     *            Y location.
     * @param z
     *            Z location.
     * @param world
     *            The world object.
     * @since 1.2.5
     */
    public static void logLastPos(final String name, final double x, final double y, final double z, final World world) {
        double xPos = Math.round(x * 100.0) / 100.0;
        double yPos = Math.round(y * 100.0) / 100.0;
        double zPos = Math.round(z * 100.0) / 100.0;
        String position = Double.toString(xPos) + "," + Double.toString(yPos) + "," + Double.toString(zPos) + ":" + McftProfiler.seeds.get(world.getName())
                + "," + world.getName();
        try {
            database.db.query("UPDATE " + database.prefix + "profiles SET lastpos = '" + position + "' WHERE username = '" + name + "';");
        } catch (Exception e) {
            log(e.toString() + "exception when logging user's last position: " + e.getMessage(), "severe");
        }
    }

    /**
     * Removes a player's last position.
     * 
     * @param name
     *            The player's name.
     * @since 1.2.5
     */
    public static void removeLastPos(final String name) {
        try {
            database.db.query("UPDATE " + database.prefix + "profiles SET lastpos = NULL WHERE username = '" + name + "';");
        } catch (Exception e) {
            log(e.toString() + "exception when removing user's last position: " + e.getMessage(), "severe");
        }
    }

    /**
     * Gets a clean string of a position for responses.
     * 
     * @param position
     *            McftProfiler position.
     * @return Friendly formatted position.
     * @since 1.2.5
     */
    public static String getString(final String position) {
        return getX(position) + ", " + getY(position) + ", " + getZ(position) + " in world " + getWorld(position) + " (" + getSeed(position) + ")";
    }

    /**
     * Gets the X location from a position.
     * 
     * @param position
     *            McftProfiler position.
     * @return X location as string.
     * @since 1.2.5
     */
    public static String getX(final String position) {
        return position.split(",")[0];
    }

    /**
     * Gets the Y location from a position.
     * 
     * @param position
     *            McftProfiler position.
     * @return Y location as string.
     * @since 1.2.5
     */
    public static String getY(final String position) {
        return position.split(",")[1];
    }

    /**
     * Gets the Z location from a position.
     * 
     * @param position
     *            McftProfiler position.
     * @return Z location as string.
     * @since 1.2.5
     */
    public static String getZ(final String position) {
        return position.split(",")[2].split(":")[0];
    }

    /**
     * Gets the seed of a map from a position.
     * 
     * @param position
     *            McftProfiler position.
     * @return World seed.
     * @since 1.2.5
     */
    public static String getSeed(final String position) {
        return position.split(":")[1].split(",")[0];
    }

    /**
     * Gets the name of a world from a position.
     * 
     * @param position
     *            McftProfiler position.
     * @return World name.
     * @since 1.2.5
     */
    public static String getWorld(final String position) {
        return position.split(",")[3];
    }
}
